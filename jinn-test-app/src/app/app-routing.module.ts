import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JinnSchedulingComponent } from './jinn-scheduling/jinn-scheduling.component';
import { FaqComponent } from './faq/faq.component';

const routes: Routes = [
  {
    path: "",
    component: JinnSchedulingComponent
  },  
  {
    path: "faq",
    component: FaqComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
